<?php

return array(

    'ng_money' => array(

        'allowCurrencies' => NULL,
        'excludeCurrencies' => NULL,

    ),

    'view_manager' => array(
        'template_map' => array(
            'netglue-money/form/money-fieldset' => __DIR__ . '/../view/netglue-money/form/money-fieldset.phtml',
        ),
    ),

);
