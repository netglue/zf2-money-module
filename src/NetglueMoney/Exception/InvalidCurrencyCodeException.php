<?php
namespace NetglueMoney\Exception;

class InvalidCurrencyCodeException extends InvalidArgumentException
{
}
