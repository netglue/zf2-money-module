<?php

namespace NetglueMoney\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
